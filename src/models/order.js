module.exports = (sequelize, DataTypes) => {

    const Order = sequelize.define('order', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },        
    },
    {
        timestamps: false,
    });
    
    
    Order.associate = models => {
        Order.belongsTo(models.User, { foreignKey: 'user_id' });
        /*
        Order.belongsToMany(models.Product, {
            through: 'OrderProducts',
            as: 'products',
            foreignKey: 'order_id',
            otherKey: 'product_id',
        });
        */
    };
    
    
    return Order;
    
};
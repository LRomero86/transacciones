module.exports = (sequelize, DataTypes) => {

    const payMethod = sequelize.define('payMethod', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        payType: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },

    },
    {
        timestamps: false,
    });

    return payMethod;
}
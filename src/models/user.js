module.exports = (sequelize,DataTypes) => {

    const User = sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        user: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING(250),
            allowNull: false,
        },
        is_admin: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        is_logged: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }
    },
    {
        timestamps: false,
        
        /*
        classMethods: {
            associate: models => {
                User.hasOne(models.Order, { foreignKey: 'user_id' });
            }
        }
        */
        
        
    });
    
    User.associate = models => {
        User.hasOne(models.Order, { onDelete: 'cascade', foreignKey: 'user_id' });
    };
        

    return User;
};
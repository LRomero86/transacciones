const {Sequelize} = require('sequelize');
const {database} = require('../config');

const transaccionesdb = new Sequelize(database.database,database.username,database.password,
    {
        host: database.host,
        dialect: database.dialect,
    });

const db = {};

db.Sequelize = Sequelize;
db.transaccionesdb = transaccionesdb;

db.User = require('./user')(transaccionesdb, Sequelize);
db.PayMethod = require('./payMethod')(transaccionesdb, Sequelize);
db.Order = require('./order')(transaccionesdb, Sequelize)
db.Product = require('./product')(transaccionesdb, Sequelize);
db.OrderProduct = require('./orderProduct')(transaccionesdb, Sequelize);

/** Associations **/

db.User.hasOne(db.Order, { foreignKey: 'user_id' });
db.Order.belongsTo(db.User, { foreignKey: 'user_id' });


db.PayMethod.hasOne(db.Order, { foreignKey: 'payMethod_id' });
db.Order.belongsTo(db.PayMethod, { foreignKey: 'payMethod_id' });


db.Order.belongsToMany(db.Product, {
    through: db.OrderProduct,
    
    foreignKey: 'order_id',
    otherKey: 'product_id',
});

db.Product.belongsToMany(db.Order, {
    through: db.OrderProduct,
    
    foreignKey: 'product_id',
    otherKey: 'order_id',
});
/*
db.OrderProduct.belongsToMany(db.Product, { foreignKey: 'order_id', through: db.OrderProduct });
db.OrderProduct.belongsToMany(db.Order, { foreignKey: 'product_id', through: db.OrderProduct });
*/

/*
db.Product.belongsToMany(db.Order, { foreignKey: 'product_id', through: db.OrderProduct });
db.Order.belongsToMany(db.Product, { foreignKey: 'order_id', through: db.OrderProduct });
*/

//db.transaccionesdb.sync({ alter: true });

module.exports = db;
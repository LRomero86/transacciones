module.exports = (sequelize, DataTypes) => {
    
    const Product = sequelize.define('product', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        productName:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        cost: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
    },
    {
        timestamps: false,
        /*
        classMethods: {
            associate: function(models) {
                Product.belongsToMany(models.Orders, { foreignKey: 'product_id', through: models.OrderProducts });
            }
        }
        */
    });

    
    Product.associate = models => {
        Product.belongsToMany(models.Order, {
            through: 'OrderProducts',
            as: 'products',
            foreignKey: 'product_id',
            otherKey: 'order_id',
        });
    };

    return Product;
};
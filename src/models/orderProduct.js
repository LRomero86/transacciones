module.exports = (sequelize, DataTypes) => {

    const OrderProduct = sequelize.define('orderProduct', {        
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        
        //order id FK
        /*
        order_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'orders',
                key: 'id',
                //allowNull: false,
            }
        },
        */

        //product id FK
        /*
        product_id: {
            type: DataTypes.INTEGER,
            references: {
                model: 'products',
                key: 'id',
                //allowNull: false,
            },
        },
        */

        quantity: {
            type: DataTypes.INTEGER(99),
            allowNull: false,
        },
    },
    {
        timestamps: false,
    });

    return OrderProduct;
};
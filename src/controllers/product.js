const db = require('../models');
const Product = db.Product;


const getListProduct = async (req, res) => {

    const listProduct = await Product.findAll();
    res.send(listProduct)

}

const createNewProduct = async (req, res) => {

    const newProduct = await Product.create(req.body);
    res.send(newProduct);

}


const updateProduct = async (req, res) => {

    const modProduct = await Product.update(req.body, 
        {
            where:{
                id:req.params.productId,
            }
        });
    
    const findProduct = await Product.findByPk(req.params.productId);
    
    res.send(findProduct);

};


const deleteProduct = async (req, res) => {

    const delProduct = await Product.destroy(
        {
            where: {
                id:req.params.productId,
            }
        })
        .then(function(DP) {
            
            if(DP === 1) {
                res.status(202).json({ message: 'Producto eliminado correctamente' });
            }
        })
        .catch(function(error) {
            res.status(201).json(error);
        });
};


module.exports = {
    getListProduct,
    createNewProduct,
    updateProduct,
    deleteProduct,
}
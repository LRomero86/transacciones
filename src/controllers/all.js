const db = require('../models');
const Product = db.Product;
const User = db.User;
const payMethod = db.PayMethod;


/*
const getListProduct = async (req, res) => {

    const listProduct = await Product.findAll();
    res.send(listProduct)

}
*/

const createAll = async (req, res) => {

    if (req.body.load == 1) {

        const newUser = await User.bulkCreate([
            {
                user: 'Admin',
                password: '123',
                is_admin: true,
                is_logged: false,
            },
            {
                user: 'Pepe',
                password: 'asd',
                is_admin: false,
                is_logged: false,
            },
            {
                user: 'juana',
                password: 'qwe',
                is_admin: false,
                is_logged: false,
            },
            {
                user: 'Luis',
                password: '456',
                is_admin: false,
                is_logged: false,
            },
            {
                user: 'María',
                password: '789',
                is_admin: false,
                is_logged: false,
            },
        ]);

        const newProduct = await Product.bulkCreate([
            {
                productName: 'Ramen',
                cost: 200
            },
            {
                productName: 'Fideos',
                cost: 150
            },
            {
                productName: 'Salmón',
                cost: 350
            },
            {
                productName: 'Ensalada',
                cost: 120
            },
            {
                productName: 'Arroz',
                cost: 225
            },
        ]);

        const newPayMethod = await payMethod.bulkCreate([
            {
                payType: 'Crédito',
            },
            {
                payType: 'Débito',
            },
            {
                payType: 'Efectivo',
            },            
        ])
     
        res.send('TODO OK');
    }
}


module.exports = {
    createAll,
}
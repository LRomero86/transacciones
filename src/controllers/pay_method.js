const db = require('../models');
const payMethod = db.PayMethod;

const getPayMethod = async (req, res) => {
    
    const listPayMethod = await payMethod.findAll();
    
    res.send(listPayMethod);
};


const createNewPayMethod = async (req, res) => {

    const newPayMethod = await payMethod.create(req.body);
    
    res.send(newPayMethod);

};


const updatePayMethod = async (req, res) => {

    const modPayMethod = await payMethod.update(req.body,
        {
            where: {
                id:req.params.payMethodId,
            }
        });

        const findPayMethod = await PayMethod.findByPk(req.params.payMethodId);

        res.send(findPayMethod);
}


const deletePayMethod = async (req, res) => {


    const delPayMethod = await payMethod.destroy(
        {
            where: {
                id:req.params.payMethodId,
            }
        })
        .then(function(DPM) {
            if(DPM === 1) {
                res.status(202).json({ message: 'Forma de pago eliminada' });
            }
        })
        .catch(function(error) {
            res.status(201).json(error);
        });
};

module.exports = {
    getPayMethod,
    createNewPayMethod,
    updatePayMethod,
    deletePayMethod,
}
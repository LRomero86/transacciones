const { Product } = require('../models');
const db = require('../models');
const order = require('../models/order');
const Order = db.Order;
const OrderProduct = db.OrderProduct;


const getListOrder = async (req, res) => {

    try {

        const listOrder = await db.Order.findAll({
            include: [{
                model: Product,
                attributes: ['id', 'productName'],
                through: { model: db.OrderProduct, attributes: ['quantity'] }
            }]
        });
        res.send(listOrder)

    } catch (error) {
        
        console.log(error.toString());
        res.send(error);

    }


}


const createNewOrder = async (req, res) => {

    const t = await Order.sequelize.transaction();

    
    try {
        
        const newOrder = await Order.create(
            {
                user_id:req.body.user_id,
            },
            {
                transaction: t,
            });
        
        for (let list_prod of req.body.products){

            orProd = {
                order_id: newOrder.id,
                product_id: list_prod.id,
                quantity: list_prod.quantity,
            }
            

            const newOrderProduct = await OrderProduct.create(orProd, { transaction: t, });
            console.log(newOrderProduct);
        }
        
        /*
        for (let i = 0; i < req.body.products.length; i++) {
            
            const orProd = {
                order_id: newOrder.id,
                product_id: req.body.products[i].id,
                quantity: req.body.products[i].quantity,
            }

            const newOrderProduct = await OrderProduct.create(orProd, { transaction: t, });

            console.log(newOrderProduct);
            
        };
        */
        

        
        await t.commit();

        const NO = await db.Order.findByPk(newOrder.id, 
            {
                include: [{
                    model: Product,
                    attributes: ['id', 'productName'],
                    through: { model: db.OrderProduct, attributes: ['quantity'] }
                }]

        });   

        res.json(NO);

    } catch (error) {

        console.log(error.toString());
        await t.rollback();
        res.send(error);

    };

};


const updateOrder = async (req, res) => {

    const findOrder = await Order.findByPk(req.params.orderId);

    const t = await Order.sequelize.transaction();

    try {

        const modOrder = await Order.update(req.body, 
            {
                where:{
                    id:req.params.orderId,
                }
            },
            {
                transaction: t,
            });
        
            OrderProduct.destroy({
                where: {
                    order_id: req.params.orderId,
                },
            },
            {
                transaction: t,
            });

            for (let list_prod of req.body.products){

                orProd = {
                    order_id: newOrder.id,
                    product_id: list_prod.id,
                    quantity: list_prod.quantity,
                }
    
                const newOrderProduct = await OrderProduct.create(orProd, { transaction: t, });
                console.log(newOrderProduct);
            }

            /*
            for (let i = 0; i < req.body.products.length; i++) {
                
                const ordProd = {
                    order_id: req.params.orderId,
                    product_id: req.body.products[i].id,
                    quantity: req.body.products[i].quantity,
                }
    
                const newOrderProduct = await OrderProduct.create(ordProd, { transaction: t, });
    
                console.log(newOrderProduct);
                
            };
            */

            await t.commit();

            const UO = await db.Order.findByPk(req.params.orderId,
                {
                    include: [{
                        model: Product,
                        attributes: ['id', 'productName'],
                        through: { model: db.OrderProduct, attributes: ['quantity'] }
                    }]
    
            });

            res.send(UO);

    } catch (error) {

        console.log(error.toString());
        await t.rollback();
        res.send(error);

    }

};


const deleteOrder = async (req, res) => {

    const delOrder = await Order.destroy({
        where: {
            id:req.params.orderId,
        }
    })
    .then(function(DO) {
        
        if(DO === 1) {
            res.status(202).json({ message: 'Orden eliminada correctamente' });
        }
    })
    .catch(function(error) {
        res.status(201).json(error);
    });
};


module.exports = {
    getListOrder,
    createNewOrder,
    updateOrder,
    deleteOrder,
}
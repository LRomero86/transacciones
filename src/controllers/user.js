const db = require('../models');
const User = db.User;
//const isLogged = require('../middlewares/middlewareUsers');

const getUserList = async (req, res) => {

    const listUser = await User.findAll();
    res.send(listUser);
};

const createNewUser = async (req, res) => {
  
    const newUser = await User.create(req.body);
  
    res.send(newUser);
};

const updateUser = async (req, res) => {

    const modUser = await User.update(req.body, 
        {
            where:{
                id:req.params.userId,
            }
        });

        console.log(req.params.userId);

    
    const findUser = await User.findByPk(req.params.userId);

        res.send(findUser);
};


const deleteUser = async (req, res) => {

    const delUser = await User.destroy({
        where: {
            id:req.params.userId,
        }
    })
    .then(function(DU) {
        
        if(DU === 1) {
            res.status(202).json({ message: 'Usuario eliminado correctamente' });
        }
    })
    .catch(function(error) {
        res.status(201).json(error);
    });
};


const login = async (req, res) => {

    const reqUser = req.body;
    console.log(reqUser);
    
    const loginUser = await User.findOne({
        where: {
            user:reqUser.user,
            password:reqUser.password,
        }
    });

    if(loginUser) {
        loginUser.is_logged = true;
        res.send(loginUser);
    }else {
        res.send('No existe usuario');
    }
}



module.exports = {
    getUserList,
    createNewUser,
    updateUser,
    deleteUser,
    login,
}
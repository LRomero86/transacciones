const db = require('../models');
const User = db.User;

// Login User

const activeUser = async (req, res, next) => {
    
    const foundUserActive = await User.findOne({
        where:{
            is_logged:true,
        },
    });

    if(foundUserActive) {        
        console.log('Ya hay un user logueado');
        res.send('Ya hay un usuario logueado');
        
    } else {
        console.log('TODO OK');
        next();
    }
}

const isLogged = async (req, res, next) => {

    const activeUser = res.locals.activeUser;

       console.log('LALALA ', activeUser);

    if(activeUser.is_logged == true) {
        next();
    } else {
        res.send('No hay usuario conectado');
    };
}

const isAdmin = async (req, res, next) => {
    
    const foundUserAdmin = await User.findOne({
        where:{
            is_admin:true,
        },
    });

    console.log("esto es: ", User);


    if (foundUserAdmin) {
        console.log('Es admin');
        next();
    } else {
        res.send('No es admin')
    }
}



module.exports = {
    activeUser,
    isLogged,
    isAdmin,
}
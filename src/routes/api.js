const router = require('express').Router();
const apiUsers = require('./api/users');
const apiProducts = require('./api/products')
const apiOrders = require('./api/orders');
const apiPayMethods = require('./api/payMethods');
const apiAll = require('./api/all');

//const {isLogged} = require('../middlewares/isLogged');

router.use('/users', apiUsers);
router.use('/products', apiProducts);
router.use('/orders', apiOrders);
router.use('/pay_methods', apiPayMethods);
router.use('/all', apiAll)

module.exports = router;
const router = require('express').Router();
const controller = require('../../controllers/all');


/** Listar Órdenes **/
//router.get('/', controller.getListOrder);

/** Crear orden **/
router.post('/', controller.createAll);

/** Modificar orden **/
//router.put('/:orderId', controller.updateOrder);

/** Eliminar orden **/
//router.delete('/:orderId', controller.deleteOrder);



module.exports = router;
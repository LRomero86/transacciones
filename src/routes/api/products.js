const router = require('express').Router();
const controller = require('../../controllers/product');


/** Listar Productos **/
router.get('/', controller.getListProduct);

/** Crear Producto **/
router.post('/', controller.createNewProduct);

/** Actualizar producto **/
router.put('/:productId', controller.updateProduct);

/** Eliminar producto **/
router.delete('/:productId', controller.deleteProduct);



module.exports = router;
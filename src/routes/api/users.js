const router = require('express').Router();
const controller = require('../../controllers/user');
const {isLogged} = require('../../middlewares/isLogged');
const middlewareUser = require('../../middlewares/middlewareUsers');

/** Listar usuarios **/
router.get('/', middlewareUser.isLogged, middlewareUser.isAdmin, controller.getUserList);

/** Crear usuario **/
router.post('/', controller.createNewUser);

/** Modificar usuario **/
router.put('/:userId', controller.updateUser);

/** Eliminar usuario **/
router.delete('/:userId', controller.deleteUser);

//router.post('/login', isLogged, controller.login);
router.post('/login', middlewareUser.activeUser, controller.login);

//seguir revisando middleware de logueo. NO FUNCIONA

module.exports = router;
const router = require('express').Router();
const controller = require('../../controllers/pay_method');


/** Listar métodos de pago **/
router.get('/', controller.getPayMethod);

router.post('/', controller.createNewPayMethod);

router.put('/:payMethodId', controller.updatePayMethod);

router.delete('/:payMethodId', controller.deletePayMethod);


module.exports = router;
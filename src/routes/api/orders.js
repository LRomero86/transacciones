const router = require('express').Router();
const controller = require('../../controllers/order');


/** Listar Órdenes **/
router.get('/', controller.getListOrder);

/** Crear orden **/
router.post('/', controller.createNewOrder);

/** Modificar orden **/
router.put('/:orderId', controller.updateOrder);

/** Eliminar orden **/
router.delete('/:orderId', controller.deleteOrder);



module.exports = router;
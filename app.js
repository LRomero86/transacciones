require('dotenv').config();
const express = require('express');
const app = express();

const apiRouter = require('./src/routes/api');
const PORT = process.env.PORT || 3000;

const db = require('./src/models');
//require('./src/models/associations');

db.transaccionesdb.sync({ force: false })
    .then(() => {
        console.log('Tablas sincronizadas');
    });


app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use('/api', apiRouter);






app.listen(PORT, () => {
    console.log('server listening in PORT', PORT);
});